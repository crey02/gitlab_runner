#!/bin/sh
docker run --rm -t -i -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock --name gitlab-runner gitlab_runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image lmodolo/r_dev:3.5.1 \
  --url "https://gitlab.biologie.ens-lyon.fr/"  \
  --registration-token "$1" \
  --description "docker-runner for r_dev:3.5.1" \
  --tag-list "docker, r, devtools" \
  --run-untagged \
  --locked="false"

