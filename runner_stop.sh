#!/bin/sh
docker stop $(docker ps -a -q --filter ancestor=gitlab_runner --format="{{.ID}}")
